
package entidade;

public class Caminhao {
    private String descricao;

    public enum Tipo {
        
        CAMINHAO(0, "Caminhão"),
        CARRETA(1, "Carreta"),
        BITREM(2, "Bitrem");
        
        final String descricao;
        final int sequencial;
 
        private Tipo (int sequencial, String descricao) {
            this.descricao = descricao;
            this.sequencial = sequencial;
        }

       private String getDescricao() {
            return descricao;
        }
        
        public int getSequencial(){
            return sequencial;
        }

        @Override
        public String toString() {
            return getDescricao();
        }
    }


    private Caminhao tipo;
    private String Placa; 
    private String Marca; 
    private String Modelo;
    private int Ano;  

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Caminhao getTipo() {
        return tipo;
    }

    public void setTipo(Caminhao tipo) {
        this.tipo = tipo;
    }

    public String getPlaca() {
        return Placa;
    }

    public void setPlaca(String Placa) {
        this.Placa = Placa;
    }

    public String getMarca() {
        return Marca;
    }

    public void setMarca(String Marca) {
        this.Marca = Marca;
    }

    public String getModelo() {
        return Modelo;
    }

    public void setModelo(String Modelo) {
        this.Modelo = Modelo;
    }

    public int getAno() {
        return Ano;
    }

    public void setAno(int Ano) {
        this.Ano = Ano;
    }

    public Caminhao(String descricao, Caminhao tipo, String Placa, String Marca, String Modelo, int Ano) {
        this.descricao = descricao;
        this.tipo = tipo;
        this.Placa = Placa;
        this.Marca = Marca;
        this.Modelo = Modelo;
        this.Ano = Ano;
    }
    
}
